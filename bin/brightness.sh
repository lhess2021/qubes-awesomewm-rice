#!/bin/bash

# bc with the -l flag imports the standard math library

brightnessDir='/sys/class/backlight/acpi_video0'
actualBrightnessFile='actual_brightness'
maxBrightnessFile='max_brightness'

brightness="$(cat $brightnessDir/$actualBrightnessFile)"
maxBrightness="$(cat $brightnessDir/$maxBrightnessFile)" # should be 48000
decimalValue="$(echo "$brightness/$maxBrightness" | bc -l | head -c 3)"

roundToNearestFive() {
	decimal="$1"                         # 0.86
	num="$(echo $decimal | tr -d .)"     # 86
	tensDigit="$(echo $num | head -c 1)" # 8
	onesDigit="$(echo $num | tail -c 2)" # 6
	case $onesDigit in
		[0-2]) # 02 -> 00, 80 -> 80, 81 -> 80, 90 -> 90
			roundedOnes=0
			roundedTensChange=0
			;;
		[3-7]) # 83 -> 85, 87 -> 85
			roundedOnes=5
			roundedTensChange=0
			;;
		[8-9]) # 88 -> 90, 
			roundedOnes=0
			roundedTensChange=1
			;;
		*)
			roundedOnes='?'
			roundedTensChange=0
			;;
	esac
	roundedOnesDigit="$roundedOnes"
	roundedTensDigit="$(( $tensDigit + $roundedTensChange ))"

	roundedNum="$roundedTensDigit$roundedOnesDigit"
	echo $roundedNum
}

# Get percentage value
if [ $decimalValue == '1.0' ]; then
	percent='100%' # Catch this before using roundToNearestFive
else
	#percent="$(echo $decimalValue | tr -d .)%"
	percent="$(roundToNearestFive $decimalValue)%"
fi

# Get icon
if [ "$brightness" == "$maxBrightness" ]; then
	icon='󰃠'
elif [ "$brightness" -gt $(( $maxBrightness / 2 )) ]; then
	icon='󰃟'
else
	icon='󰃞'
fi


echo "$icon $percent"
