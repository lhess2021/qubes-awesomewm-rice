#!/bin/sh

lbin='/home/lhess/.local/bin'

autorun() {
	if ! pgrep -f "$1" ;
	then
		$@ &
	fi
}

if [[ "$(whoami)" == "lhess" ]]; then
	#autorun "xrandr --output HDMI1 --auto"
	#autorun "xrandr --output HDMI1 --right-of eDP1"
	bash -c "xinput --set-prop 'PNP0C50:00 04F3:311D Touchpad' 'libinput Tapping Enabled' 1"
	bash -c "xinput --set-prop 'ETPS/2 Elantech Touchpad' 'libinput Tapping Enabled' 1"
	autorun 'xset r rate 300 50'
	autorun 'qvm-start sys-vpn-proton'
	autorun 'qvm-run work /home/user/nextcloud-sync/projects/split-passman/systemd-start.sh'
	autorun '/home/lhess/.local/bin/start-sys-audio.sh'
	autorun '/home/lhess/.local/bin/startup.sh'
	autorun "$lbin/set-next-wallpaper.sh"
fi
