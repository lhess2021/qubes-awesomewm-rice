local awful = require("awful")
local naughty = require("naughty")

local config = require("config")

local dmenuBin = config.dirs.dmenuBin
local localBin = config.dirs.localBin
local scriptsDir = config.dirs.scriptsDir

-- Helper functions to run a script or command
local function qvm_run(vm, command)
    awful.spawn.with_shell([[qvm-run ]] .. vm .. [[ ']] .. command .. [[']])
end

local function qvm_run_startapp(vm, app)
    -- qvm-run --service -- wpi qubes.StartApp+brave-browser
    awful.spawn.with_shell([[qvm-run -q -a --service -- ]] .. vm .. [[ qubes.StartApp+]] .. app)
end

local function dmenu_run(dir, script)
    awful.spawn.with_shell(dir .. '/' .. script)
end

local function script_run(script)
    awful.spawn.with_shell(scriptsDir .. '/' .. script)
end

-- Functions to run actual keybind command
-- Work
local function emacsclient() qvm_run('work', "emacsclient -cn -a vim") end

-- School
local function brave() qvm_run("school", "brave-browser") end

-- Wpi
local function wpiFirefox() qvm_run("wpi", "firefox-esr") end
local function wpiBrave() qvm_run_startapp("wpi", "brave-browser --disable-gpu") end
local function wpiBrave() qvm_run_startapp("wpi", "brave-browser --disable-gpu") end
local function wpiBraveCrashFix() qvm_run_startapp("wpi", "brave-browser --enable-chrome-browser-cloud-management") end
local function decodeOutlookSafelink() qvm_run("wpi", "bash /home/user/.local/bin/decode-outlook-safelink-handler.sh") end

-- Thunderbird
local function thunderbird() qvm_run("thunderbird", "thunderbird") end

-- Signal
local function signal() qvm_run("signal", "/opt/Signal/signal-desktop") end

-- Dmenu
local function desktopRun() dmenu_run(localBin, "qubes-j4-dmenu-desktop") end
local function qubeManager() dmenu_run(dmenuBin, "dm-qube-manager.sh") end
local function settings() dmenu_run(dmenuBin, "dm-settings.sh") end
local function netVMSwitcher() dmenu_run(dmenuBin, "dm-netvm-switcher.sh") end
local function browserLauncher() dmenu_run(dmenuBin, "dm-browsers.sh") end
local function emacsDictionary() dmenu_run(dmenuBin, "dict.sh") end

-- Scripts
local function yubikeyAttach() awful.spawn.with_shell(localBin .. "/yubikey-handler.sh attach") end
local function yubikeyDetach() awful.spawn.with_shell(localBin .. "/yubikey-handler.sh detach") end
local function tripleMonitors() script_run("triple-monitors.sh") end
local function lockScreen() awful.spawn.with_shell("xscreensaver-command -lock") end
local function shutdown() awful.spawn.with_shell("sudo shutdown now") end
local function pauseAll() awful.spawn.with_shell("qvm-pause --all") end
local function unpauseAll()
    awful.spawn.with_shell("qvm-unpause --all")
    -- Kill openvpn process and rerun /rw/config/rc.local
    awful.spawn.with_shell(localBin .. "/qvm-unpause-vpn-handler.sh")
end

-- Split-Passman
local function wpiPasswordTotp() awful.spawn.with_shell(localBin .. "/split-passman/wpi.sh") end

-- Keybind function tables
local binding_functions = {}

binding_functions.vmWork = {
    emacsclient = emacsclient,
}

binding_functions.vmSchool = {
    brave = brave,
}

binding_functions.vmWpi = {
    firefox = wpiFirefox,
    --brave = wpiBrave,
    brave = wpiBraveCrashFix,
    decodeOutlookSafelink = decodeOutlookSafelink,
}

binding_functions.vmSignal = {
    signal = signal,
}

binding_functions.dmenu = {
    desktopRun = desktopRun,
    qubeManager = qubeManager,
    settings = settings,
    netVMSwitcher = netVMSwitcher,
    browserLauncher = browserLauncher,
    emacsDictionary = emacsDictionary,
}

binding_functions.scripts = {
    yubikeyAttach = yubikeyAttach,
    yubikeyDetach = yubikeyDetach,
    tripleMonitors = tripleMonitors,
    lockScreen = lockScreen,
    shutdown = shutdown,
    pauseAll = pauseAll,
    unpauseAll = unpauseAll,
}

binding_functions.splitPassman = {
    wpiPasswordTotp = wpiPasswordTotp,
}

return binding_functions

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
